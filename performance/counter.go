package performance

import (
	"fmt"
	"time"
)

type Lap struct {
	Name  string
	Start time.Time
	Last  time.Time
	Time  time.Time
}
type Laps []Lap
func (l Laps) Last() Lap {
	return l[len(l)-1]
}
func (l Lap) String() string {
	return fmt.Sprintf("%s: %s\t%s", l.Name,l.Time.Sub(l.Start),l.Time.Sub(l.Last))
}
func (l Lap) Print()  {
	fmt.Println(l)
}

func StartChrono(name string) func(lap string) Laps {
	laps := make(Laps, 1)
	lastLap := Lap{
		Name:  name,
		Start: time.Now(),
		Last:  time.Now(),
		Time:  time.Now(),
	}
	laps[0] = lastLap
	return func(lap string) Laps {
		lastLap = Lap{
			Name:  lap,
			Start: lastLap.Start,
			Last:  lastLap.Time,
			Time:  time.Now(),
		}
		laps = append(laps,lastLap)
		return laps
	}
}