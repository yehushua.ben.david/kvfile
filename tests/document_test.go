package tests

import (
	"fmt"
	"gitlab.com/yehushua.ben.david/kvfile"
	"os"
	"testing"
)

func Test_document_load(t *testing.T) {
	kvDBFile := GetTmpFile()
	kvf, err := kvfile.NewKvFileConnector(kvDBFile)
	defer os.Remove(kvDBFile)
	if CheckError(t, err) {
		return
	}
	d := kvf.NewDocument()
	d.SetValue("id", d.ID[:])
	d.SetString("txt", "Text")
	d.Save()
	d2, err := kvf.LoadDocument(d.ID)
	if CheckError(t, err) {
		return
	}
	v1,_:=d.GetString("txt")
	v2,_:= d2.GetString("txt")
	if  v1 !=v2 {
		t.Errorf("%s is not %s",v1,v2)
	}
}

func Test_document_json(t *testing.T) {
	kvDBFile := GetTmpFile()
	kvf, err := kvfile.NewKvFileConnector(kvDBFile)
	defer os.Remove(kvDBFile)
	if CheckError(t, err) {
		return
	}
	d := kvf.NewDocument()
	d.SetString("Name","Arnaud")
	bs,err:=d.Json()
	if CheckError(t, err) {
		return
	}
}
