package tests

import (
	"fmt"
	"math/rand"
	"os"
	"path/filepath"
	"testing"
	"time"
)

func GetTmpFile() string {
	return filepath.Clean(fmt.Sprintf("%s/%x.kvf", os.TempDir(), time.Now().UnixNano()))
}
func CheckError(t *testing.T, e error) bool {
	if e != nil {
		t.Error(e)
		return true
	}
	return false
}
func DummyData() []byte {
	size := 1024 + rand.Int()%1024
	data := make([]byte, size)
	for i := 0; i < size; i++ {
		data[i] = byte(rand.Int())
	}
	return data
}
type ZERO struct{}
var NULL = ZERO{}
func NOP(...interface{}) {
}

func Comp(b1, b2 []byte) int {
	s := len(b1) - len(b2)
	if s > 0 {
		return 1
	}
	if s < 0 {
		return -1
	}

	for i := range b1 {
		if b1[i] == b2[i] {
			continue
		}
		if b1[i]-b2[i] > 0 {
			return 1
		} else {
			return -1
		}
	}
	return 0
}
