package tests

import (
	"crypto/sha256"
	"errors"
	"fmt"
	"gitlab.com/yehushua.ben.david/kvfile"
	"os"
	"strconv"
	"sync"
	"testing"
	"time"
)

func Test_connectors_create_file(t *testing.T) {
	kvDBFile := GetTmpFile()
	_, err := kvfile.NewKvFileConnector(kvDBFile)
	defer os.Remove(kvDBFile)
	if CheckError(t, err) {
		return
	}
	_, err = os.Stat(kvDBFile)
	if CheckError(t, err) {
		return
	}

}
func Test_connectors_fail_to_connect_file(t *testing.T) {
	kvDBFile := GetTmpFile()
	f, err := os.OpenFile(kvDBFile, os.O_RDWR|os.O_CREATE, 0666)
	defer os.Remove(kvDBFile)
	defer f.Close()
	if CheckError(t, err) {
		return
	}
	f.Write(DummyData())
	_, err = kvfile.NewKvFileConnector(kvDBFile)
	if err == nil {
		if CheckError(t, errors.New("invalid format file should raise an Error")) {
			return
		}
	} else {
		t.Logf("Normal as expected Error:'%s'", err)
	}
}

func Test_connectors_set_get_data_back(t *testing.T) {
	kvDBFile := GetTmpFile()
	kvf, err := kvfile.NewKvFileConnector(kvDBFile)
	if CheckError(t, err) {
		return
	}
	defer os.Remove(kvDBFile)

	key := fmt.Sprintf("%x", time.Now().UnixNano())
	value := fmt.Sprintf("%x", time.Now().UnixNano())
	if CheckError(t, kvf.SetString([32]byte{}, key, value)) {
		return
	}
	if retValue := kvf.GetString([32]byte{}, key); retValue != value {
		t.Error(errors.New(fmt.Sprintf("The return value <%s>  should be <%s>", retValue, value)))
	}
	kvf.Close()
	kvf, err = kvfile.NewKvFileConnector(kvDBFile)
	if CheckError(t, err) {
		return
	}
	if retValue := kvf.GetString([32]byte{}, key); retValue != value {
		t.Error(errors.New(fmt.Sprintf("The return value <%s>  should be <%s>", retValue, value)))
	}
	kvf.Close()
}

func Test_connectors_multi_thread_access(t *testing.T) {
	kvDBFile := GetTmpFile()
	kvf, err := kvfile.NewKvFileConnector(kvDBFile)
	defer os.Remove(kvDBFile)
	if CheckError(t, err) {
		return
	}
	var nbThread byte = 25
	nbOfEntries := 1000
	ids := make([][32]byte, nbThread)
	wg := sync.WaitGroup{}
	for i := 0; i < int(nbThread); i++ {
		ids[i] = [32]byte{byte(i)}
		wg.Add(1)
		go func(id [32]byte) {
			defer wg.Done()
			for j := 0; j <= nbOfEntries; j++ {
				kvf.Lock()
				kvf.SetString(id, "Step", strconv.Itoa(j))
				kvf.UnLock()
			}
		}(ids[i])
	}
	wg.Wait()
	lastStep := strconv.Itoa(nbOfEntries)
	errorList := make([]error, 0)
	for idx, id := range ids {
		if idRst := kvf.GetString(id, "Step"); idRst != lastStep {
			errorList = append(errorList, errors.New(fmt.Sprintf("thread nb(%d) Step=%s, Objectif: %s",
				idx, idRst, lastStep)))
		}
	}
	for _, e := range errorList {
		t.Error(e)
	}
}

func Test_connectors_get_all_ids(t *testing.T) {
	kvDBFile := GetTmpFile()
	kvf, err := kvfile.NewKvFileConnector(kvDBFile)
	defer os.Remove(kvDBFile)
	if CheckError(t, err) {
		return
	}
	idsHashMap := make(map[[32]byte]ZERO, 0)
	for i := 0; i < 1000; i++ {
		id := sha256.Sum256([]byte(strconv.Itoa(i)))
		idsHashMap[id] = NULL
		kvf.SetValue(id, "id", id[:])
	}
	idsInBase := kvf.GetAllIds()
	for _, id := range idsInBase {
		if _, ok := idsHashMap[id]; !ok {
			t.Error(errors.New(fmt.Sprintf("%x not found", id)))
			return
		}
		value, err := kvf.GetValue(id, "id")
		if CheckError(t, err) {
			return
		}
		if Comp(value, id[:]) != 0 {
			t.Errorf("%x not equal %x", id, value)
			return
		}
	}
}

func Test_connector_newID_no_double(t *testing.T) {
	IDs := make(map[[32]byte]ZERO)
	// made with 10_000_000 during the Dev phase
	for i := 0; i < 1_000_000; i++ {
		a := kvfile.NewID()
		if _, ok := IDs[a]; ok {
			t.Errorf("ID already exists: \n\t%x", a)
			return
		}
		IDs[a] = NULL
	}
}


func Test_connector_delete(t *testing.T) {
	kvDBFile := GetTmpFile()
	kvf, err := kvfile.NewKvFileConnector(kvDBFile)
	defer os.Remove(kvDBFile)
	if CheckError(t, err) {
		return
	}
	aUid := kvfile.NewID()
	kvf.SetValue(aUid, "key1", []byte("val1"))
	kvf.SetValue(aUid, "key2", []byte("val2"))
	kvf.SetValue(aUid, "key3", []byte("val3"))
	kvf.SetValue(aUid, "key4", []byte("val4"))
	kvf.DelKey(aUid, "key1")
	keys , err := kvf.GetValues(aUid)
	if CheckError(t, err) {
		return
	}
	if _, found := keys["key1"]; found {
		t.Error("key1 not deleted")
	}

	if _, found := keys["key2"]; !found {
		t.Error("key2 deleted")
	}

	if _, found := keys["key3"]; !found {
		t.Error("key3 deleted")
	}

	if _, found := keys["key4"]; !found {
		t.Error("key4 deleted")
	}

	kvf.Close()

	 kvfReloaded, err := kvfile.NewKvFileConnector(kvDBFile)
	if CheckError(t, err) {
		return
	}
	keys , err = kvfReloaded.GetValues(aUid)
	if CheckError(t, err) {
		return
	}
	if _, found := keys["key1"]; found {
		t.Error("key1 not deleted")
	}

	if _, found := keys["key2"]; !found {
		t.Error("key2 deleted")
	}

	if _, found := keys["key3"]; !found {
		t.Error("key3 deleted")
	}

	if _, found := keys["key4"]; !found {
		t.Error("key4 deleted")
	}
	kvfReloaded.DelUid(aUid)
	if len(kvfReloaded.GetAllIds()) > 0 {
		t.Error("Still in Index the ID: " + kvfile.Uid2Str(aUid) )
	}
	if len(kvfReloaded.GetAllIds()) > 0 {
		t.Error("Still in Index the ID: " + kvfile.Uid2Str(aUid) )
	}
	kvfReloaded, err = kvfile.NewKvFileConnector(kvDBFile)
	if CheckError(t, err) {
		return
	}
	fmt.Println()
	dt,err:=kvfReloaded.GetValues(aUid)
	if CheckError(t, err) {
		return
	}
	if len(dt)>0 {
		t.Error("Still data: " + kvfile.Uid2Str(aUid) )
	}

}
