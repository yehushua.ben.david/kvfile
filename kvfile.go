package kvfile

import (
	"crypto/sha256"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"math/rand"
	"os"
	"path/filepath"
	"sync"
)

const HeaderLen = 3*8 + 32

type TKeyIndex map[[32]byte]int64
type TMapIdIndex map[[32]byte]TKeyIndex
type TMapKeyIndex map[[32]byte][]int64

type Connector struct {
	LMutex   sync.Mutex
	File     *os.File
	IdIndex  TMapIdIndex
	KeyIndex TMapKeyIndex
	Active   bool
}
type Header struct {
	Allocated int64
	Used      int64
	Key       int64
	Id        [32]byte
}



var HexValues [256]byte
func init() {
	for i,v := range "0123456789abcdef" {
		HexValues[byte(v)]=byte(i)
	}
	for i,v := range "ABCDEF" {
		HexValues[byte(v)]=byte(10+i)
	}
}
func Xtob(s string) byte {
	b1 := HexValues[s[0]]
	b2 := HexValues[s[1]]
	return (b1 << 4) | b2
}
func Uid2Str(uid [32]byte) string {
	return fmt.Sprintf("%x",uid)
}
func Str2Uid(sid string) [32]byte {
	var bb [32]byte
	for i:=0 ; i< len(sid) / 2 ; i++ {
		v:=Xtob(sid[i*2:i*2+2]+"  ")
		bb[i%32] += v
	}
	return bb
}


type KvDocument struct {
	ID   [32]byte
	Conn *Connector
	DeletedKeys map[string]struct{}
	Data map[string][]byte
}
func (k *Connector) NewDocument() *KvDocument {
	return &KvDocument{
		ID:   NewID(),
		Conn: k,
		DeletedKeys: make(map[string]struct{}),
		Data: make(map[string][]byte),
	}
}
func (d *KvDocument) DelKey(key string) {
	if _,found:=d.Data[key]; found {
		delete(d.Data,key)
		d.DeletedKeys[key]= struct{}{}
	}
}
func (d *KvDocument) Json() ([]byte,error) {
	rst := make(map[string]string,0)
	rst["__ID__"]=Uid2Str(d.ID)
	for key,value := range d.Data {
		rst[key]= string(value)
	}
	return json.Marshal(rst)
}
func (d *KvDocument) Load() error {
	d.Data = make(map[string][]byte)
	d.DeletedKeys=make(map[string]struct{})
	var err error
	d.Data, err = d.Conn.GetValues(d.ID)
	return err
}
func (d *KvDocument) LoadSafe() error {
	d.Conn.Lock()
	defer d.Conn.UnLock()
	return d.Load()
}
func (d *KvDocument) Insert() error {
	d.ID=NewID()
	return d.Save()
}
func (d *KvDocument) New() {
	d.ID = NewID()
	d.Data = make(map[string][]byte)
d.DeletedKeys= make(map[string]struct{})
}
func (d *KvDocument) InsertSafe() error {
	d.Conn.Lock()
	defer d.Conn.UnLock()
	return d.Insert()
}
func (d *KvDocument) Save(keys ...string) error {
	for key,_ := range d.DeletedKeys{
		err:=d.Conn.DelKey(d.ID,key)
		if err != nil {
			return err
		}
	}
	if len(keys) > 0 {
		for _, key := range keys {
			if value, ok := d.Data[key]; ok {
				err := d.Conn.SetValue(d.ID, key, value)
				if err != nil {
					return err
				}
			}
		}
		return nil
	}
	for key, value := range d.Data {
		err := d.Conn.SetValue(d.ID, key, value)
		if err != nil {
			return err
		}
	}
	return nil
}
func (d *KvDocument) SaveSafe(keys ...string) error {
	d.Conn.Lock()
	defer d.Conn.UnLock()
	return d.Save(keys...)
}

func (d *KvDocument) Delete() error {
	return d.Conn.DelUid(d.ID)
}
func (d *KvDocument) DeleteSafe() error {
	d.Conn.Lock()
	defer d.Conn.UnLock()
	return d.Delete()
}

func (d *KvDocument) GetValue(s string)([]byte,bool) {
	if d.Data==nil {
		d.Data=make(map[string][]byte)
		d.DeletedKeys= make(map[string]struct{})
		return nil,false
	}
	v,ok := d.Data[s]
	return v,ok
}

func (d *KvDocument) GetString(s string)(string,bool) {
	if d.Data==nil {
		d.Data=make(map[string][]byte)
		d.DeletedKeys= make(map[string]struct{})
		return "",false
	}
	v,ok := d.Data[s]
	return string(v),ok
}

func (d *KvDocument) SetValue(s string, value []byte) {
	if d.Data==nil {
		d.Data=make(map[string][]byte)
		d.DeletedKeys= make(map[string]struct{})

	}
	delete(d.DeletedKeys,s)
	d.Data[s]= value
}

func (d *KvDocument) SetString(key string, value string) {
	d.SetValue(key,[]byte(value))
}
func NewID() [32]byte {
	a := [32]byte{}
	for i := 0; i < 8; i++ {
		n := rand.Uint32()
		a[i*4] = byte(n)
		a[i*4+1] = byte(n >> 8)
		a[i*4+2] = byte(n >> 16)
		a[i*4+3] = byte(n >> 24)
	}
	return a
}
func NewKvFileConnector(resource string) (*Connector, error) {
	k := &Connector{}
	return k, k.Connect(resource)
}
func (k *Connector) Lock() {
	k.LMutex.Lock()
}
func (k *Connector) UnLock() {
	k.LMutex.Unlock()
}
func (k *Connector) BuildIndex() error {
	k.IdIndex = make(TMapIdIndex, 0)
	k.KeyIndex = make(TMapKeyIndex, 0)
	k.File.Seek(0, 0)
	h := &Header{}
	for {
		pos, err := k.File.Seek(0, 1)
		if err != nil {
			return err
		}
		h, err = k.ReadHeader()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			break
		}
		if h.Used == 0 {
			k.File.Seek(h.Allocated-HeaderLen, 1)
			continue
		}
		if h.Used > h.Allocated {
			return errors.New("bad row header")
		}
		if h.Key+HeaderLen > h.Used {
			return errors.New("bad row header")
		}

		bKey := make([]byte, h.Key)
		n, err := k.File.Read(bKey)
		if int64(n) != h.Key || err != nil {
			return errors.New("Fail reading Key")
		}
		keyBKey := sha256.Sum256(bKey)
		var keyIndex TKeyIndex
		var ok bool
		if keyIndex, ok = k.IdIndex[h.Id]; !ok {
			keyIndex = make(TKeyIndex, 0)
			k.IdIndex[h.Id] = keyIndex
		}
		keyIndex[keyBKey] = pos
		var listPos []int64
		if listPos, ok = k.KeyIndex[keyBKey]; !ok {
			listPos = make([]int64, 0)
		}
		k.KeyIndex[keyBKey] = append(listPos, pos)

		k.File.Seek(h.Allocated-HeaderLen-h.Key, 1)
	}
	return nil
}
func (k *Connector) GetFromIndex(uid [32]byte, key string) int64 {
	keyIndex, found := k.IdIndex[uid]
	if !found {
		return -1
	}
	pos, found := keyIndex[sha256.Sum256([]byte(key))]
	if !found {
		return -1
	}
	return pos
}
func (k *Connector) GetValue(uid [32]byte, key string) ([]byte, error) {
	pos := k.GetFromIndex(uid, key)
	if pos == -1 {
		return nil, errors.New("not found")
	}
	k.File.Seek(pos, 0)
	h := &Header{}
	binary.Read(k.File, binary.LittleEndian, h)
	k.File.Seek(h.Key, 1)
	bValue := make([]byte, h.Used-h.Key-HeaderLen)
	_, err := k.File.Read(bValue)
	return bValue, err
}

func (k *Connector) GetString(uid [32]byte, key string) string {
	bs, err := k.GetValue(uid, key)
	if err != nil {
		return ""
	}
	return string(bs)
}

func (k *Connector) SetString(uid [32]byte, key string, value string) error {
	return k.SetValue(uid, key, []byte(value))
}
func (k *Connector) SetStrings(uid [32]byte, data map[string]string) error {
	for key, value := range data {
		err := k.SetString(uid, key, value)
		if err != nil {
			return err
		}
	}
	return nil
}
func (k *Connector) GetAll() []map[string]string {
	rst := make([]map[string]string, 0, len(k.IdIndex))
	for uid := range k.IdIndex {
		rst = append(rst, k.GetStrings(uid))
	}
	return rst
}
func (k *Connector) DelKey(uid [32]byte, key string) error {
	pos := k.GetFromIndex(uid, key)
	if pos == -1 {
		return nil
	}
	k.File.Seek(pos,0)
	h,err:=k.ReadHeader()
	if err != nil {
		return err
	}
	h.Used=0
	k.File.Seek(pos,0)
	err=binary.Write(k.File, binary.LittleEndian, h)
	if err != nil {
		return err
	}
	delete(k.IdIndex[uid],sha256.Sum256([]byte(key)))
	if len(k.IdIndex[uid]) == 0 {
		delete(k.IdIndex,uid)
	}
	return nil
}
func (k *Connector) DelUid(uid [32]byte) error {
	keys,found := k.IdIndex[uid]
	if !found {
		return nil
	}
	for _,pos := range keys {
		k.File.Seek(pos,0)
		h,err:=k.ReadHeader()
		if err != nil {
			return err
		}
		h.Used=0
		k.File.Seek(pos,0)
		err=binary.Write(k.File, binary.LittleEndian, h)
		if err != nil {
			return err
		}
	}
	delete(k.IdIndex,uid)
	return nil
}

func (k *Connector) SetValue(uid [32]byte, key string, value []byte) error {
	bKey := []byte(key)
	h := Header{
		Allocated: int64(HeaderLen + len(bKey) + len(value)),
		Used:      int64(HeaderLen + len(bKey) + len(value)),
		Key:       int64(len(bKey)),
		Id:        uid,
	}

	pos := k.GetFromIndex(uid, key)
	if pos > -1 {
		k.File.Seek(pos, 0)
		hPrevious, err := k.ReadHeader()
		if err != nil {
			return err
		}
		if hPrevious.Allocated >= h.Used {
			h.Allocated = hPrevious.Allocated
		} else {
			k.File.Seek(pos, 0)
			hPrevious.Used = 0
			binary.Write(k.File, binary.LittleEndian, hPrevious)
			pos, _ = k.File.Seek(0, 2)
		}
	} else {
		pos, _ = k.File.Seek(0, 2)
	}
	k.File.Seek(pos, 0)
	binary.Write(k.File, binary.LittleEndian, h)
	k.File.Write(bKey)
	k.File.Write(value)
	k.SetIndex(uid, sha256.Sum256(bKey), pos)

	return nil
}
func (k *Connector) SetIndex(uid [32]byte, bkey [32]byte, pos int64) {
	keyIndex, ok := k.IdIndex[uid]
	if !ok {
		k.IdIndex[uid] = TKeyIndex{bkey: pos}
		return
	}
	keyIndex[bkey] = pos
}
func (k *Connector) Close() {
	k.File.Sync()
	k.File.Close()
}
func (k *Connector) Connect(resource string) error {
	file, _ := filepath.Abs(resource)
	f, err := os.OpenFile(file, os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		return err
	}
	k.File = f
	k.Active = true
	return k.BuildIndex()
}

func (k *Connector) ReadHeader() (*Header, error) {
	h := &Header{}
	err := binary.Read(k.File, binary.LittleEndian, h)
	return h, err
}

type Row struct {
	Header *Header
	Key    string
	Value  []byte
}

func (r *Row) ToString() string {
	return fmt.Sprintf("%d\t%d\t%d\t%x\t%s\t%x(%s)", r.Header.Allocated, r.Header.Used, r.Header.Key, r.Header.Id, r.Key, r.Value, string(r.Value))
}

var nb int

func (k *Connector) ReadRow() (*Row, error) {
	r := Row{}
	var err error
	r.Header, err = k.ReadHeader()
	if err != nil {
		return nil, err
	}
	if r.Header.Used == 0 {
		k.File.Seek(r.Header.Allocated-HeaderLen, 1)
		r.Key = ""
		r.Value = []byte{}
		return &r, nil
	}
	bs := make([]byte, r.Header.Used-HeaderLen)

	_, err = k.File.Read(bs)
	k.File.Seek(r.Header.Allocated-r.Header.Used, 1)
	if err != nil {
		return nil, err
	}
	r.Key = string(bs[:r.Header.Key])
	r.Value = bs[r.Header.Key:]
	return &r, nil
}

func (k *Connector) PrintFile() {
	k.File.Seek(0, 0)
	fmt.Println("Row\tAllocated\tUsed\tKeyPos\tID\tkey\tValue")

	for {
		pos, _ := k.File.Seek(0, 1)
		r, err := k.ReadRow()
		if err != nil {
			return
		}
		fmt.Println(pos, "\t", r.ToString())
	}
}

func (k *Connector) GetStrings(uid [32]byte) map[string]string {
	result := map[string]string{}
	if keyIndex, ok := k.IdIndex[uid]; ok {
		for _, pos := range keyIndex {
			k.File.Seek(pos, 0)
			r, _ := k.ReadRow()
			result[r.Key] = string(r.Value)
		}
	}
	return result
}

func (k *Connector) GetValues(uid [32]byte) (map[string][]byte, error) {
	result := map[string][]byte{}
	if keyIndex, ok := k.IdIndex[uid]; ok {
		for _, pos := range keyIndex {
			k.File.Seek(pos, 0)
			r, err := k.ReadRow()
			if err != nil {
				return result, err
			}
			result[r.Key] = r.Value
		}
	}
	return result, nil
}

func (k *Connector) GetAllIds() [][32]byte {
	ids := make([][32]byte, 0, len(k.IdIndex))
	for id := range k.IdIndex {
		ids = append(ids, id)
	}
	return ids
}

func (k *Connector) LoadDocument(id [32]byte) (*KvDocument,error) {
	dd,err := k.GetValues(id)
	if err != nil {
		return nil,err
	}
	d:=k.NewDocument()
	d.ID=id
	d.Data=dd
	return d,nil
}
